var express = require("express");
var app = express();
var server = require("http").createServer(app);
var fs = require("fs");
server.listen(process.env.PORT || 3000);
var bodyParser = require('body-parser');
var server = require('./src/servers/server')

server.connect();

app.get('/api', function (req, res) {
    res.statusCode.json(200, { msg: 'OK' });
})

app.get('/api/authors',  (req, res) => {
    var start = Number.parseInt(req.query.start)
    var limit = Number.parseInt(req.query.limit)

    server.authorFind(start,limit,(result)=>{
        res.status(200).json(result)
    })
})

app.get('/api/storys',  (req, res) => {
   var start = Number.parseInt(req.query.start)
    var limit = Number.parseInt(req.query.limit)

    server.storyFind(start,limit,(result)=>{
        res.status(200).json(result)
    })
})
app.get('/api/story',  (req, res) => {
   var id = Number.parseInt(req.query.id)

    server.storyFindById(id,(result)=>{
        res.status(200).json(result)
    })
})

app.get('/api/story2type',  (req, res) => {
   var id = Number.parseInt(req.query.id_type)
   var start = Number.parseInt(req.query.start)
    var limit = Number.parseInt(req.query.limit)

    server.storyFindToTypeId(id,start,limit,(result)=>{
        res.status(200).json(result)
    })
})

app.get('/api/chaper', (req,res) => {
        var id = Number.parseInt(req.query.id_chaper)    

        server.chaperFind2Id(id,(result)=>{
            res.status(200).json(result)
        })
    })
app.get("/", function (req, res) {
    console.log(MONGOLAB_URI);
    res.sendFile(__dirname + "/src/index.html");
});