var MongoClient = require('mongodb').MongoClient;
var config = require('./config.js')
var ex = module.exports
let MONGOLAB_URI = config.MONGO_LAB_URI;
var db;
var colAuthor = 'author', colStory = 'story', colChaper = 'chaper', colType = 'type';

_generator = (err, data) => {
    var responeJson = { code: 0, mgs: 'error', data: null }
    if (err) {
        console.log("find error: " + err)
        responeJson = { code: 0, mgs: 'error', data: null }
    }
    else {
        responeJson = { code: 1, mgs: 'success', data: data }
    }
    return responeJson
}

ex.connect = () => {
    MongoClient.connect(MONGOLAB_URI, function (err, database) {
        if (err) throw err
        console.log("connected")
        db = database
    })
}
ex.authorFind = (start, limit, callback) => {

    db.collection(colAuthor).find({ Id: { $gt: start } }, { limit: limit })
        .toArray(function (err, result) {
            callback(_generator(err, result))
        })
}

ex.storyFind = (start, limit, callback) => {

    db.collection(colStory).find({ Id: { $gt: start } }, { limit: limit })
        .toArray(function (err, result) {
            callback(_generator(err, result))
        })
}

ex.storyFindById = (id, callback) => {

    db.collection(colStory).aggregate([
        { $match: { Id: id } },
        {
            $lookup: {
                from: colAuthor,
                localField: "IdAuthor",
                foreignField: "Id",
                as: "Author"
            }
        },
        {
            $lookup: {
                from: colType,
                localField: "IdType",
                foreignField: "Id",
                as: "Type"
            }
        },
        {
            $lookup: {
                from: colChaper,
                localField: "Id",
                foreignField: "IdStory",
                as: "Chaper"
            }
        },
        {
            $project: {
                "Id": 1, "NameStory": 1, "Chaps": 1, "Downloads": 1, "DateUpdate": 1, "Description": 1, "Cover": 1
                , "Author.Author": 1, "Type.NameType": 1, "Chaper.Id": 1, "Chaper.NameChaper": 1
            }
        }
    ], (err, result) => {
        callback(_generator(err, result))
    })
}

ex.storyFindToTypeId = (idType, start, limit, callback) => {
    db.collection(colStory).find({ IdType: idType, RowNumber: { $gt: start } }, { limit: limit })
        .toArray(function (err, result) {
            callback(_generator(err, result))
        })
}
ex.chaperFind2Id = (idChaper, callback) => {
    db.collection(colChaper).find({ Id: idChaper }).toArray(
        (err, result) => {
            callback(_generator(err, result))
        }
    )
}